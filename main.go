package main

import (
	cfg "config"
	"net/http"
	"os"
	"strconv"
	"time"
	p "xmlParser"

	"github.com/gorilla/sessions"
	log "github.com/sirupsen/logrus"

	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

var store = sessions.NewCookieStore([]byte("sometsdfg54345AE%&SR&(%§A$&IUj56sj34ret"))

func init() {
	if cfg.Config.Logger.Format == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{})
	}

	if cfg.Config.Logger.Output == "console" {
		log.SetOutput(os.Stdout)
	} else {
		log.SetOutput(&lumberjack.Logger{
			Filename:   cfg.Config.Logger.Filename,
			MaxSize:    cfg.Config.Logger.Rotation.MaxFileSizeMB, // megabytes
			MaxBackups: cfg.Config.Logger.Rotation.MaxNoOfBackups,
			MaxAge:     cfg.Config.Logger.Rotation.MaxAgeDays, //days
			Compress:   cfg.Config.Logger.Rotation.Compress,   // disabled by default
		})
	}

	if cfg.Config.Logger.Level == "debug" {
		log.SetLevel(log.DebugLevel)
	} else if cfg.Config.Logger.Level == "info" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}

}

func main() {

	ticker := time.NewTicker(500 * time.Millisecond)
	go func() {
		for range ticker.C {
			readXML()
		}
	}()

	router := NewRouter()
	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))))

	log.Fatal(http.ListenAndServe(cfg.Config.Server.Host+":"+strconv.Itoa(cfg.Config.Server.Port), router))

}

func readXML() {
	p.UpdateData()
}
