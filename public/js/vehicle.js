const maxNumDataPoints = 100;

const chartColorArray  = {
    Treibstoff: "#f4921c", //fuel
    Ölrettich: "#49b484", //Oilreddish
    Gerste: "#897247", //Barley
    Dünger: "#7fc032", //fertiolizer
    Weizen: "#ceab4f",//wheat 
    Raps: "#9cc251", //Rape
    Sojabohnen: "#81852e", //Soy
    Kartoffeln: "#8c634f", //Potatoes
    Sonnenblumen: "#eacb22", //sunflower
    Körnermais: "#e89a2e", //Maize
    Zuckerrüben: "#f2e8e6", //sugarbeets
    Pappel: "#687d70", //poplar/cottonwood
    Zuckerrohr: "#d9a38f", //Cane
    Gras: "#009933",    //grass
    Hackschnitzel: "#7b482f", //chips
    Default: "#cc6600"
}


$( document ).ready(function() {

    $.get( "/public/tpl/tpl_vehicle.html", function( data ) {
        tpl_VehicleFrame= Handlebars.compile(data);
      });

      $.get( "/public/tpl/tpl_vehicledata.html", function( data ) {
          tpl_VehicleData= Handlebars.compile(data);
        });

      updateData();
});

var tpl_VehicleFrame = ""
var chartData = [];
var vhclCharts = [];

var chartConfig = { type: 'line', events: [],  scales: { xAxes: [{ type: 'time' }]}  };

function updateData() {
    $.getJSON('/json', function(data) {

        var vehicleData = data.Vehicles.Vehicle
        for (i = 0; i < vehicleData.length; i++) { 
            var cardID = "vhcl_" + vehicleData[i].ID;


            if ($('#' + cardID).length){
                //Div exists
                updateDiv(vehicleData[i]);
            } else {
                //Create Div
                createDiv(vehicleData[i]);
            }

            updateChartData(vehicleData[i])

        }

        setTimeout(updateData, 500);
    });
}

function updateDiv(vhclData) {

    var htmlData = ""
    var chartID = "chart_" + vhclData.ID;
    var cardID = "vhcl_" + vhclData.ID;
    vhclData.Display = displayVehicle(vhclData);

    if(vhclData.Display) 
    {
        var tmpData = tpl_VehicleData({
            FuelLevel: tanklistToString(vhclData.Tanks.Tank),
            Attachments: attachmentsToString(vhclData.Attachments.Attachment)})
        $('#'+cardID + " .content").html(tmpData);
        $('#'+cardID + " .card-title").html(vehicleTitle(vhclData));
        
     
    }
    else {
        $('#'+cardID).remove()
        return;
    }


}


function createDiv(vhclData) {

    var htmlData = ""
    var chartID = "chart_" + vhclData.ID;
    var cardID = "vhcl_" + vhclData.ID;
    vhclData.Display = displayVehicle(vhclData);

    if(vhclData.Display) 
    {
        var tmpData = tpl_VehicleData({
            FuelLevel: tanklistToString(vhclData.Tanks.Tank),
            Attachments: attachmentsToString(vhclData.Attachments.Attachment)})

        htmlData += tpl_VehicleFrame({
            title: vehicleTitle(vhclData),
            data: tmpData,
            canvasID: chartID,
            VehicleID: cardID,
        }); 
    }
    else {
   //    text += vhclData.Name + "<br>";
    }

    $('#vehicleData').append(htmlData);

}

function updateChartData(vhclData) {
    if(vhclData.Tanks==null)
        return;

    if(vhclData.Tanks.Tank==null)
            return;

    //Iterate through Tanks
    for (let tIdx = 0; tIdx < vhclData.Tanks.Tank.length; tIdx++) {
    
        var tankData = vhclData.Tanks.Tank[tIdx];

        if(tankData.TypeID == 0)
            break;

        updateChartGui(vhclData.ID, tankData.Type, {y: getPercentage(tankData.Level, tankData.Capacity), x: new Date()});
   
    }

    removeOldSeries(vhclData.ID, vhclData.Tanks.Tank);

}


function removeOldSeries(vID, Tanks) {

    var chart = vhclCharts[vID];

    if(chart != null) {
        for (let idx = 0; idx < chart.data.datasets.length; idx++) {
            const e = chart.data.datasets[idx];
            var found = false;

            for (let tIdx = 0; tIdx < Tanks.length; tIdx++) {
                const tnk = Tanks[tIdx];
                if(tnk.Type == e.label) {
                    found = true;
                    break;

                }
            }

            if(!found){
                chart.data.datasets.splice(idx, 1);
            }
        }
    }
}

function getPercentage(level, capacity) {
    return (level / capacity * 100)
}


function  updateChartGui(vID, label, data) {

    if(vhclCharts[vID] == null) {
        var chartID = "chart_" + vID;
        if ($('#' + chartID).length){
            var ctx = document.getElementById(chartID).getContext('2d');
            vhclCharts[vID] = new Chart(ctx, {
                type: 'line',
                options: {
                  scales: {
                    xAxes: [{
                      display: false,
                      type: 'time',
                      time: {
                        displayFormats: {
                          second: 'MMM YYYY'
                        }
                      }
                    }],
            
                    yAxes: [{
                      display: true,
                      ticks: {
                        min: 0,
                        max: 100
                      }
                    }]
                  },
                  legend: {
                    display: true,
                    boxWidth: 10,
                  }
                }
              });
        }
    }

    if(vhclCharts[vID] == null) 
        return;

    var series = null
    var chart = vhclCharts[vID];

    //Check  if Series exist
    for (let idx = 0; idx < chart.data.datasets.length; idx++) {
        const e = chart.data.datasets[idx];
        if(e.label == label){
            series = chart.data.datasets[idx]
            break;
        }
    }

    if(series == null) {
        addSeriesToChart(chart, label, data)
    }
    else {
        updateChartSeries(series, data)

        chart.update(.1);
    }

}


function updateChartSeries(series, data) {
    
    series.data.push(data);

    while (series.data.length > maxNumDataPoints) {
        series.data.shift();
    }

}


function addSeriesToChart(chart, label, data) {
    
    var color=chartColorArray["default"]

    if(chartColorArray[label] != null)
        color = chartColorArray[label]


    chart.data.datasets.push({
	    label: label,
        fill: false,
        backgroundColor: color,
        borderColor: color,
        pointRadius:1,
        data: [data]
    });

    chart.update(.1);

}

function vehicleTitle(vhcl) {
    result = vhcl.Name

    if(vhcl.Speed.Speed>0) {
        result += " (" + Math.round(vhcl.Speed.Speed) + " " +vhcl.Speed.Unit+ ")"
    }

    if(vhcl.IsHired) {
        result = "*" + result
    }

    if(vhcl.FieldNo >= 0) {
        result += " F" + vhcl.FieldNo
    }

    return result
}

function attachmentsToString(attList) {
    var output = ""

    if(attList==null)
        return ""

    for (var i = 0; i < attList.length; i++) { 
        output += attList[i].Name + "<br>" 
        if(attList[i].Attr != "") {
            output += attList[i].Attr + "<br>" 
        }
    }

    return output
}

function tanklistToString(tankList) {
    var output = ""

    for (var i = 0; i < tankList.length; i++) { 
        if(tankList[i].Level > 0) 
        output += tankToString(tankList[i]) + "<br>" 
    }

    return output
}

function tankToString(tb) {

        return tb.Type + ": " + ((tb.Level / tb.Capacity * 100).toFixed(0)) + "% (" + Math.round(tb.Level) +"/"+ Math.round(tb.Capacity) + ")"; 
 
}


function displayVehicle(vehicleData) {


    var tanks = vehicleTankFilled(vehicleData.Tanks.Tank);

    if(tanks || vehicleData.IsControlled || vehicleData.IsHired|| vehicleData.IsCurrent)
        return true


    return false

}


function vehicleTankFilled(tankData) {

    if(tankData != null){
        for (k = 0; k < tankData.length; k++) { 
            if(tankData[k].Capacity > 0 &&  tankData[k].Level > 0 &&  tankData[k].Type != "Treibstoff" )
            {
                return true
            }
        }
    }
    return false
}