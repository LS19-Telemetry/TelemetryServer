const updateIntervalMS = 500

$( document ).ready(function() {


      updateData();
});

function updateData() {
    $.getJSON('/json', function(data) {


        setTimeout(updateData, updateIntervalMS);
    });
}
