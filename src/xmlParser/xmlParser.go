package xmlParser

import (
	cfg "config"
	"encoding/xml"
	"io/ioutil"
	"os"
	"regexp"
	"time"

	log "github.com/sirupsen/logrus"

	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

type Tank struct {
	Capacity float32 `xml:"Capacity"`
	Level    float32 `xml:"Level"`
	Type     string  `xml:"Type"`
	TypeID   int     `xml:"TypeID"`
}

type Attachment struct {
	Name string `xml:"Name"`
	Attr string `xml:"Attr_1",omitempty`
}

type Speed struct {
	Speed float32 `xml:"Speed"`
	Unit  string  `xml:"Unit"`
}

type SellingPoint struct {
	Name     string `xml:"Name"`
	FillType []struct {
		TypeID         int     `xml:"TypeID"`
		Type           string  `xml:"Type"`
		Price          float32 `xml:"Price"`
		Delta          float32 `xml:"Delta"`
		Multiplier     float32 `xml:"Multiplier"`
		EffectivePrice float32 `xml:"EffectivePrice"`
	}
}

type Silo struct {
	FillType []struct {
		TypeID int    `xml:"TypeID"`
		Type   string `xml:"Type"`
		Level  int    `xml:"Level"`
	}
}

type Vehicle struct {
	IsActive      bool   `xml:"IsActive"`
	IsTrain       bool   `xml:"IsTrain"`
	HelperActive  bool   `xml:"HelperActive"`
	ViVehicleName string `xml:"viVehicleName"`
	Name          string `xml:"Name"`
	Desc          string `xml:"Desc"`
	CruiseControl struct {
		Status    int  `xml:"Status"`
		Speed     int  `xml:"Speed"`
		SpeedSent int  `xml:"SpeedSent"`
		IsActive  bool `xml:"IsActive"`
	} `xml:"CruiseControl"`
	MotorStarted bool `xml:"MotorStarted"`
	OnField      bool `xml:"OnField"`
	Field        int  `xml:"Field"`
	Speed        int  `xml:"Speed"`
	InWater      bool `xml:"InWater"`
	Damaged      bool `xml:"Damaged"`
	Attachments  int  `xml:"Attachments"`
	Pos          struct {
		Z float32 `xml:"Z"`
		X float32 `xml:"X"`
	} `xml:"Pos,omitempty"`
	Blocked bool `xml:"Blocked,omitempty"`
	Fill    struct {
		Level    string `xml:"Level"`
		Capacity string `xml:"Capacity"`
		Percent  string `xml:"Percent"`
		Inverted string `xml:"Inverted"`
		Title    string `xml:"Title"`
		Name     string `xml:"Name"`
		ID       string `xml:"ID"`
	} `xml:"Fill,omitempty"`
}

type GameData struct {
	XMLName  xml.Name `xml:"TelemetryData"`
	Vehicles struct {
		Vehicle []Vehicle `xml:"Vehicle"`
	} `xml:"Vehicles"`
	/*
		Prices struct {
			SellingPoints []SellingPoint `xml:"SellingPoint"`
		} `xml:"Prices"`

		Silos struct {
			Silo []Silo `xml:"Silo"`
		} `xml:"Silos"`

		Animals struct {
			Sheeps struct {
				PalletLevel    float32 `xml:"PalletLevel"`
				PalletCapacity float32 `xml:"PalletCapacity"`
				Text           string  `xml:"Text"`
				Name           string  `xml:"Name"`
				Amount         float32 `xml:"Amount"`
				Productivity   float32 `xml:"Productivity"`
				Cleanliness    float32 `xml:"Cleanliness"`
			} `xml:"Sheeps"`
			Pigs struct {
				Text         string  `xml:"Text"`
				Name         string  `xml:"Name"`
				Amount       float32 `xml:"Amount"`
				Productivity float32 `xml:"Productivity"`
				Cleanliness  float32 `xml:"Cleanliness"`
			} `xml:"Pigs"`
			Cows struct {
				Text         string  `xml:"Text"`
				Name         string  `xml:"Name"`
				Amount       float32 `xml:"Amount"`
				Productivity float32 `xml:"Productivity"`
				Cleanliness  float32 `xml:"Cleanliness"`
			} `xml:"Cows"`
		} `xml:"Animals"`
	*/
	LastUpdate time.Time
	Source     string
}

var TeleData GameData

func init() {
	if cfg.Config.Logger.Format == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{})
	}

	if cfg.Config.Logger.Output == "console" {
		log.SetOutput(os.Stdout)
	} else {
		log.SetOutput(&lumberjack.Logger{
			Filename:   cfg.Config.Logger.Filename,
			MaxSize:    cfg.Config.Logger.Rotation.MaxFileSizeMB, // megabytes
			MaxBackups: cfg.Config.Logger.Rotation.MaxNoOfBackups,
			MaxAge:     cfg.Config.Logger.Rotation.MaxAgeDays, //days
			Compress:   cfg.Config.Logger.Rotation.Compress,   // disabled by default
		})
	}

	if cfg.Config.Logger.Level == "debug" {
		log.SetLevel(log.DebugLevel)
	} else if cfg.Config.Logger.Level == "info" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}
}

func readFile() (string, error) {

	_, err := os.Stat(cfg.Config.Game.Input)
	if err == nil {
		b, err := ioutil.ReadFile(cfg.Config.Game.Input)

		if err != nil {
			log.Fatal("Cannot load XML:"+cfg.Config.Game.Input, err)
			return "", err
		}

		str := string(b)
		os.Remove(cfg.Config.Game.Input)
		return str, nil
	}

	return "ne", err

}

func cleanData(input string) (string, error) {

	re := regexp.MustCompile("Vehicle_\\d+>")
	s := "Vehicle>"
	str := re.ReplaceAllString(input, s)

	re = regexp.MustCompile("Tank_\\d+>")
	s = "Tank>"
	str = re.ReplaceAllString(str, s)

	re = regexp.MustCompile("Attachment_\\d+>")
	s = "Attachment>"
	str = re.ReplaceAllString(str, s)

	re = regexp.MustCompile("Fill_\\d+>")
	s = "Fill>"
	str = re.ReplaceAllString(str, s)

	re = regexp.MustCompile("Silo_\\d+>")
	s = "Silo>"
	str = re.ReplaceAllString(str, s)

	re = regexp.MustCompile("SellingPoint_\\d+>")
	s = "SellingPoint>"
	str = re.ReplaceAllString(str, s)

	return str, nil
}

func parseData(input string) error {

	TeleData.Vehicles.Vehicle = []Vehicle{}
	//	TeleData.Prices.SellingPoints = []SellingPoint{}
	//	TeleData.Silos.Silo = []Silo{}

	err := xml.Unmarshal([]byte(input), &TeleData)
	TeleData.LastUpdate = time.Now()
	TeleData.Source = cfg.Config.Game.Input

	return err
}

func UpdateData() {

	str, err := readFile()

	if err != nil {
		if str != "ne" {
			log.Error("Error Updating 1 " + err.Error())
		}
		return
	}

	CleanData, err := cleanData(str)
	if err != nil {
		log.Error("Error Updating 2 " + err.Error())
		return
	}

	err = parseData(CleanData)
	if err != nil {
		log.Error("Error Updating 3 " + err.Error())
		return
	}
}
