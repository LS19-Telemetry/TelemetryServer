package xmlParser

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var InputData string
var CleanData string

func TestReadFile(t *testing.T) {
	str, err := readFile()
	InputData = str

	//	t.Log("str:" + InputData)
	assert.Nil(t, err)
	assert.NotNil(t, str)

}

func TestCleanData(t *testing.T) {
	str, err := readFile()
	InputData = str
	CleanData, err := cleanData(InputData)

	t.Log("str:" + CleanData)
	assert.Nil(t, err)
	assert.NotNil(t, CleanData)

}
func TestParseData(t *testing.T) {
	str, err := readFile()
	InputData = str
	CleanData, err := cleanData(InputData)

	err = parseData(CleanData)
	assert.Nil(t, err)
	assert.NotNil(t, CleanData)

	b, _ := json.MarshalIndent(TeleData, "", "  ")
	os.Stdout.Write(b)

	t.Log("str:" + string(b))

}
